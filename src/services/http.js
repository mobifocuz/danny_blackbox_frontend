import axios from 'axios'
import localStorage from './localStorage'

// Refactor later, should we have an init method here to make it singleton..
let http = axios.create({
  baseURL: process.env.API_ENDPOINT,
  timeout: 30000,
  headers: {
    Accept: 'application/json'
  }
})

http.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${localStorage.get('access-token')}`
  return config
}, function (error) {
  return Promise.reject(error)
})

export default http
