import http from '../services/http'

export default {
  getExample () {
    return http.get('/example')
  }
}
