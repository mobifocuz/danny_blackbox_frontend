import Vue from 'vue'
import Router from 'vue-router'
import DefaultLayout from '@/layouts/Default.vue'

const Hello = () => import('@/containers/Hello.vue')

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: DefaultLayout,
      children: [
        {
          path: '',
          name: 'home',
          component: Hello
        }
      ]
    }
  ]
})
